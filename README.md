# Toy robot app

The app has been tested using **python 3.**
If you are going to use python2, you probably have some incompatibilities to run it.

It can be run using an interactive mode or a testing mode. Please, find bellow how to run either of the two options.

## Interactive mode

The terminal will ask you to enter instructions manually. 
Also, this mode draws the table board and the robot after each instruction.
  
`python3 toy_robot_app.py`

## Testing mode

The testing mode is able to proccess '.txt' files with instructions.
In the directory tests/ are available all the scenarios listed in the requirements.

`python3 toy_robot_app.py --testing-path tests/`
